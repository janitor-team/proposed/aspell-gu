aspell-gu (0.03-0-11) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 4.5.0
    + Switched to debhelper-compat.
    + Use https in Homepage.
    + Section: text->localization
    + Added 'Rules-Requires-Root' field.
  * Added debian/gitlab-ci.yml.
  * debian/rules:
    + Use --with aspell-simple.
  * Updated debian/copyright.

 -- Kartik Mistry <kartik@debian.org>  Wed, 08 Apr 2020 18:47:19 +0530

aspell-gu (0.03-0-10) unstable; urgency=low

  * debian/control:
    + Updated Vcs-* fields.
    + Updated Standards-Version to 4.2.1
    + Updated Team email address.
  * Bumped dh to 11.

 -- Kartik Mistry <kartik@debian.org>  Tue, 04 Dec 2018 10:34:42 +0530

aspell-gu (0.03-0-9) unstable; urgency=low

  [ Kartik Mistry ]
  * debian/rules:
    + Add -n option to gzip for reproducible build.
  * debian/control:
    + Bumped Standards-Version to 3.9.7 (no changes).
    + Fixed Vcs-* URLs.
  * Bumped debian/compat to 9
  * Updated debian/copyright.

 -- Kartik Mistry <kartik@debian.org>  Sun, 27 Mar 2016 20:05:23 +0530

aspell-gu (0.03-0-8) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Fixed VCS-* URLs.
    * Updated Standards-Version to 3.9.4 (no changes needed).
  * wrap-and-sort debian/control and debian/install files.

 -- Kartik Mistry <kartik@debian.org>  Fri, 04 Oct 2013 21:14:58 +0530

aspell-gu (0.03-0-7) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Vcs-* fields for new Git repository.
  * debian/copyright:
    + Updated to latest DEP-5 format.
  * Bump debian/compat to 7.
  * debian/rules:
    + Exclude /var/lib/aspell folder from md5sum calculation.

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 21 Dec 2011 19:59:04 +0530

aspell-gu (0.03-0-6) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.9.2 (no changes needed)
    + Updated Build-Depends/Depends for dh7
  * debian/copyright:
    + Updated for latest DEP-5 format
  * debian/rules:
    + Simple rules file using dh7

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 04 Jun 2011 12:16:25 +0530

aspell-gu (0.03-0-5) unstable; urgency=low

  [Kartik Mistry]
  * Updated package to use new source format 3.0 (quilt)
  * debian/control:
    + Updated Standards-Version to 3.8.4 (no changes needed)
    + Wrapped up Build-Depends-Indep, Depends
    + Updated debhelper dependency to >= 7.0.5~
  * debian/rules:
    + Used dh_prep instead of dh_clean -k

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Thu, 29 Apr 2010 01:08:51 +0530

aspell-gu (0.03-0-4) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.8.2
  * debian/copyright:
    + [Lintian] Removed versionless symlink to license
  * Sync README and Makefile.pre with upstream to remove
    changes in diff.gz

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 30 Jun 2009 10:01:59 +0530

aspell-gu (0.03-0-3) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.1
    + Updated my maintainer email address
    + Added ${misc:Depends} for debhelper dependency
  * debian/copyright:
    + [Lintian] Updated for correct copyright symbol

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 17 Jun 2009 11:39:32 +0530

aspell-gu (0.03-0-2) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated homepage as control field
    + Updated Standards-Version to 3.7.3
    + Added VCS-* field
    + Updated short description
  * debian/copyright:
    + Updated link of GPL
    + Removed reference of .bz2 compression

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 14:27:59 +0530

aspell-gu (0.03-0-1) unstable; urgency=low

  [Kartik Mistry]
  * New upstream release
  * debian/rules: fixed clean target
  * debian/copyright: updated upstream url and authors, moved copyright out of
    license section, added package copyright
  * debian/control: updated homepage entry, updated short and long
    descriptions
  * debian/watch: updated according to upstream changes

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 18 Sep 2007 12:24:56 +0530

aspell-gu (0.02-2) unstable; urgency=low

  [Kartik Mistry]
  * Changed Maintainer address to Debian-IN Team
  * debian/rules: minor cleanups
  * debian/control: updated debhelper compability

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 20 Mar 2007 11:33:37 +0530

aspell-gu (0.02-1) unstable; urgency=low

  * Initial Release (closes: #359103)

 -- Kartik Mistry <kartik.mistry@gmail.com>  Tue, 17 Oct 2006 14:00:33 +0530
